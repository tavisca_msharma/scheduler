package com.pentaho.billingProcess.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@SpringBootApplication
@ComponentScan(basePackages = "com.pentaho.*")
@EnableJpaRepositories("com.pentaho.scheduler.data")
@EntityScan("com.pentaho.scheduler.model")
public class SchedulerApplication {
	private static final Logger logger = LoggerFactory.getLogger(SchedulerApplication.class);
	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
	public static void main(String[] args) {
		SpringApplication.run(SchedulerApplication.class, args);
		logger.info("Started Scheduler @", dateTimeFormatter.format(LocalDateTime.now()));
	}
}