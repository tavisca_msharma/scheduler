package com.pentaho.publisher.controller;

import com.pentaho.scheduler.model.ClientInfo;
import com.pentaho.scheduler.repository.BillingClientInfo;
import com.pentaho.scheduler.service.IKafkaPublisher;
import com.pentaho.scheduler.util.ObjectFieldMapper;
import javafx.application.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class MessagePublisherController {

    @Autowired
    IKafkaPublisher iKafkaPublisher;

    @Autowired
    BillingClientInfo billingClientInfo;

    @Autowired
    ObjectFieldMapper objectFieldMapper;

    private final String status = "Processed";

    @GetMapping(value = "/billing/producer")
    public String producer(@RequestParam("message") String message) {
        List<ClientInfo> clientInfoList = billingClientInfo.getAllClientInfo();
        List<ClientInfo> updatedClientInfoList = new ArrayList<>();
        for(ClientInfo info : clientInfoList)
        {
            iKafkaPublisher.send(objectFieldMapper.messageFieldMapping(info));
            info.setStatus(status);
            updatedClientInfoList.add(info);
        }
        billingClientInfo.updateClientInfo(updatedClientInfoList);
        return "Message sent to the Kafka Topic Successfully";
    }

}
