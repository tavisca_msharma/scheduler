package com.pentaho.scheduler.service;

import com.pentaho.scheduler.model.DataStream;

public interface IKafkaPublisher {
    public void send(DataStream message);
}
