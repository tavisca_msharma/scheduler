package com.pentaho.scheduler.service;

public interface ITaskScheduler {

void scheduleTasks() throws InterruptedException;
}
