package com.pentaho.scheduler.serviceImpl;

import com.pentaho.scheduler.model.ClientInfo;
import com.pentaho.scheduler.repository.BillingClientInfo;
import com.pentaho.scheduler.service.IKafkaPublisher;
import com.pentaho.scheduler.service.ITaskScheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
public class TaskScheduler implements ITaskScheduler {
    private static final Logger logger = LoggerFactory.getLogger(TaskScheduler.class);
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    @Autowired
    BillingClientInfo billingClientInfo;

    @Autowired
    IKafkaPublisher iKafkaPublisher;

    @Override
    @Scheduled(cron = "${cronExpression}")
    public void scheduleTasks() throws InterruptedException {
        List<ClientInfo> clientInfoList = (List<ClientInfo>) billingClientInfo.getAllClientInfo();
        for(ClientInfo info : clientInfoList)
        {
         //iKafkaPublisher.send(info.getClientId());
         Thread.sleep(1000);
        }
        logger.info("Cron Task :: Execution Time - {}", dateTimeFormatter.format(LocalDateTime.now()));
    }
}
