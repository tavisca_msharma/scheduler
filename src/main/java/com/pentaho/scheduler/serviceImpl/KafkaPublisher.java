package com.pentaho.scheduler.serviceImpl;

import com.pentaho.scheduler.model.DataStream;
import com.pentaho.scheduler.service.IKafkaPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
public class KafkaPublisher implements IKafkaPublisher {
    private static final Logger logger = LoggerFactory.getLogger(KafkaPublisher.class);
    @Value(value = "${kafka.topic}")
    private String kafkaTopic;
    @Autowired
    private KafkaTemplate<String, DataStream> kafkaTemplate;

    @Override
    public void send(DataStream dataStream) {
        Message<DataStream> message = MessageBuilder
                .withPayload(dataStream)
                .setHeader(KafkaHeaders.TOPIC, kafkaTopic)
                .build();
        logger.debug("Publishing : "+dataStream+ " on topic "+kafkaTopic);
        kafkaTemplate.send(message);
    }
}
