package com.pentaho.scheduler.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "clientinfo")
@Data
@NoArgsConstructor
@Getter
@Setter
public class ClientInfo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "clientid")
    private String clientId;

    @Column(name = "clientname")
    private String clientName;

    @Column(name = "clientaccount")
    private String clientAccount;

    @Column(name = "accountmappedto")
    private String accountMappedTo;

    @Column(name = "billingdate")
    private Date billingDate;

    @Column(name = "status")
    private String status;

}
