package com.pentaho.scheduler.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import java.util.Date;

@Getter@Setter
@NoArgsConstructor
@ToString
public class DataStream {
    private String clientid;
    private String clientname;
    private String clientaccount;
    private String accountmappedto;
    private String billingdate;
    private String status;

}
