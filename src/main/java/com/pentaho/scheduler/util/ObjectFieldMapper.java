package com.pentaho.scheduler.util;

import com.pentaho.scheduler.model.ClientInfo;
import com.pentaho.scheduler.model.DataStream;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class ObjectFieldMapper {

    public DataStream messageFieldMapping(ClientInfo clientInfo) {
        DataStream dataStream = new DataStream();
        dataStream.setClientid(clientInfo.getClientId());
        dataStream.setClientaccount(clientInfo.getClientAccount());
        dataStream.setClientname(clientInfo.getClientName());
        dataStream.setAccountmappedto(clientInfo.getAccountMappedTo());
        dataStream.setBillingdate(clientInfo.getBillingDate().toString());
        dataStream.setStatus(clientInfo.getStatus());
        return dataStream;
    }

}
