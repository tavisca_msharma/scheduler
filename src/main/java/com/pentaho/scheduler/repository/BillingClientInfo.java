package com.pentaho.scheduler.repository;

import com.pentaho.scheduler.data.IClientBillingInfo;
import com.pentaho.scheduler.model.ClientInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BillingClientInfo {

    @Autowired
    IClientBillingInfo iClientBillingInfo;

    public List<ClientInfo> getAllClientInfo()
    {
        return iClientBillingInfo.findAll();
    }

    public void updateClientInfo(List<ClientInfo> clientInfoList)
    {
        iClientBillingInfo.saveAll(clientInfoList);
    }

}
