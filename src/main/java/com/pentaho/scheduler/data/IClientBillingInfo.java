package com.pentaho.scheduler.data;

import com.pentaho.scheduler.model.ClientInfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface IClientBillingInfo extends CrudRepository<ClientInfo,Integer> {

    List<ClientInfo> findAll();
}
